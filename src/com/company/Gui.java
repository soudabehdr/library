package com.company;

import com.mongodb.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

public class Gui extends JFrame {


    JLabel jLabel1 = new JLabel();
    JLabel jLabel2 = new JLabel();

    JButton jButton= new JButton("login");
    JTextField jTextField1= new JTextField();
    JTextArea jTextArea1= new JTextArea();

    JTextField jTextField2= new JTextField();
    JTextArea jTextArea2= new JTextArea();

    JTextField jTextField3= new JTextField();
    JTextArea jTextArea3= new JTextArea();

    JTextField jTextField4= new JTextField();
    JTextArea jTextArea4= new JTextArea();

    String fileName = new Date(System.currentTimeMillis()).toString();
    JTextArea jTextAreaDate= new JTextArea();



    public Gui(){

        JFrame jFrame= new JFrame();
        setLayout(null);
        jFrame.setBounds(0,0,1200,800);
        jFrame.setLayout(new GridLayout(1,2));
        jFrame.setTitle("Library Management System");
        jFrame.add(jLabel1);
        jFrame.add(jLabel2);
        jLabel1.setBounds(0,0,600,800);
        jLabel2.setBounds(603,0,600,800);
       /* jLabel1.setBackground(Color.GRAY);
        jLabel1.setOpaque(true);
        jLabel2.setBackground(Color.GRAY);
        jLabel2.setOpaque(true);*/



        jLabel1.setBorder(BorderFactory.createLineBorder(Color.cyan, 3));
        jLabel2.setBorder(BorderFactory.createLineBorder(Color.cyan, 3));
        jLabel1.add(jButton);

        jTextAreaDate.setFont(new Font("SansSerif", Font.ITALIC, 20));
        jTextAreaDate.setBounds(60,70,300,100);
        jTextAreaDate.setBackground(Color.cyan);
        jLabel2.add(jTextAreaDate);


        jTextField1.setBounds(120,200,420,50);
        jTextField1.setFont( new Font("SansSerif", Font.PLAIN, 20));
        jLabel1.add(jTextField1);


        jTextArea1.setText("username:");
        jTextArea1.setFont(new Font("SansSerif", Font.LAYOUT_NO_START_CONTEXT, 20));
        jTextArea1.setBounds(20,212,100,30);
        jTextArea1.setBackground(Color.cyan);
        jTextArea1.setForeground(Color.BLACK);
        jLabel1.add(jTextArea1);


        jTextField2.setBounds(120,400,420,50);
        jTextField2.setFont( new Font("SansSerif", Font.PLAIN, 20));
        jLabel1.add(jTextField2);

        jTextArea2.setText("password:");
        jTextArea2.setFont(new Font("SansSerif", Font.LAYOUT_NO_START_CONTEXT, 20));
        jTextArea2.setBounds(20,412,100,30);
        jTextArea2.setBackground(Color.cyan);
        jTextArea2.setForeground(Color.BLACK);
        jLabel1.add(jTextArea2);


       /* jTextField3.setBounds(120,200,420,50);
        jTextField3.setFont( new Font("SansSerif", Font.PLAIN, 20));
        jLabel2.add(jTextField3);


        jTextArea3.setText("IPEK:");
        jTextArea3.setFont(new Font("SansSerif", Font.LAYOUT_NO_START_CONTEXT, 20));
        jTextArea3.setForeground(Color.BLACK);

        jTextArea3.setBounds(60,212,100,30);
        jLabel2.add(jTextArea3);

        jTextField4.setBounds(120,400,420,50);
        jTextField4.setFont( new Font("SansSerif", Font.PLAIN, 20));
        jLabel2.add(jTextField4);

        jTextArea4.setText("PEK:");
        jTextArea4.setFont(new Font("SansSerif", Font.LAYOUT_NO_START_CONTEXT, 20));
        jTextArea4.setBounds(60,412,100,30);
        jLabel2.add(jTextArea4);
        jTextArea4.setForeground(Color.BLACK);
*/
        jButton.setLayout(null);
        jButton.setBounds(440,550,80,63);
        jFrame.setVisible(true);
       // jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jTextAreaDate.setText(fileName +"\n plaese waiting...  \n *************************");


        jButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {


                String username =jTextField1.getText().toString();
                String password =jTextField2.getText();


                Mongo mongo = new Mongo("localhost", 27017);
                DB db = mongo.getDB("library");
                DBCollection library = db.getCollection("member");
/*
                BasicDBObject criteria = new BasicDBObject();
                criteria.append("username", username);
                criteria.append("pass",password);
                DBCursor cur = library.find(criteria);*/

                BasicDBObject searchQuery = new BasicDBObject();
                searchQuery.put("username", username);
                searchQuery.put("password",password);

                DBCursor cur = library.find(searchQuery);



                if(cur.hasNext()){

                    jTextAreaDate.setText("you are login successfully");

                    BasicDBObject query = new BasicDBObject();
                    query.put("username", username);
                    BasicDBObject newDocument = new BasicDBObject();
                    newDocument.put("activeStatus", "login");
                    BasicDBObject updateObj = new BasicDBObject();
                    updateObj.put("$set", newDocument);
                    library.update(query, updateObj);

                    MemberWindow memberWindow= new MemberWindow();

                }
               if(!cur.hasNext()) {

                    jTextAreaDate.setText("username or password isnt valid!");

                }
            }
        });

    }

    public static void main(String[] args) {
        Gui gui = new Gui();
    }


}
