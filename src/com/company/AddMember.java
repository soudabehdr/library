package com.company;

import javax.swing.*;
import java.awt.*;

public class AddMember extends JFrame {


    JLabel jLabel1 = new JLabel();

    JTextField jTextField1= new JTextField();
    JTextArea jTextArea1= new JTextArea();

    JTextField jTextField2= new JTextField();
    JTextArea jTextArea2= new JTextArea();

    JTextField jTextField3= new JTextField();
    JTextArea jTextArea3= new JTextArea();

    JTextField jTextField4= new JTextField();
    JTextArea jTextArea4= new JTextArea();

    JTextField jTextField5= new JTextField();
    JTextArea jTextArea5= new JTextArea();

    JTextField jTextField0= new JTextField();
    JTextArea jTextArea0= new JTextArea();

    JButton jButton= new JButton("ok");

    public AddMember(){

        JFrame jFrame= new JFrame();
        jFrame.setLayout(null);
        jFrame.setBounds(0,0,1200,800);
        jFrame.setLayout(new GridLayout(1,2));
        jFrame.setTitle("Library Management System");
        jFrame.setVisible(true);
        jFrame.add(jLabel1);

        jLabel1.setBounds(0,0,600,800);
        jLabel1.setBorder(BorderFactory.createLineBorder(Color.cyan, 3));
        jLabel1.add(jButton);


        jTextField0.setBounds(120,10,420,50);
        jTextField0.setFont( new Font("SansSerif", Font.PLAIN, 20));
        jLabel1.add(jTextField0);


        jTextArea0.setText("name:");
        jTextArea0.setFont(new Font("SansSerif", Font.LAYOUT_NO_START_CONTEXT, 20));
        jTextArea0.setBounds(20,22,100,30);
        jTextArea0.setBackground(Color.cyan);
        jTextArea0.setForeground(Color.BLACK);
        jLabel1.add(jTextArea0);
//===========================================================================================================
        jTextField1.setBounds(120,100,420,50);
        jTextField1.setFont( new Font("SansSerif", Font.PLAIN, 20));
        jLabel1.add(jTextField1);


        jTextArea1.setText("usre name:");
        jTextArea1.setFont(new Font("SansSerif", Font.LAYOUT_NO_START_CONTEXT, 20));
        jTextArea1.setBounds(20,112,100,30);
        jTextArea1.setBackground(Color.cyan);
        jTextArea1.setForeground(Color.BLACK);
        jLabel1.add(jTextArea1);
//===========================================================================================================
        jTextField2.setBounds(120,170,420,50);
        jTextField2.setFont( new Font("SansSerif", Font.PLAIN, 20));
        jLabel1.add(jTextField2);


        jTextArea2.setText("password:");
        jTextArea2.setFont(new Font("SansSerif", Font.LAYOUT_NO_START_CONTEXT, 20));
        jTextArea2.setBounds(20,182,100,30);
        jTextArea2.setBackground(Color.cyan);
        jTextArea2.setForeground(Color.BLACK);
        jLabel1.add(jTextArea2);
//===========================================================================================================

        jTextField3.setBounds(120,240,420,50);
        jTextField3.setFont( new Font("SansSerif", Font.PLAIN, 20));
        jLabel1.add(jTextField3);

        jTextArea3.setText("address:");
        jTextArea3.setFont(new Font("SansSerif", Font.LAYOUT_NO_START_CONTEXT, 20));
        jTextArea3.setBounds(20,252,100,30);
        jTextArea3.setBackground(Color.cyan);
        jTextArea3.setForeground(Color.BLACK);
        jLabel1.add(jTextArea3);
//===========================================================================================================

        jTextField4.setBounds(120,310,420,50);
        jTextField4.setFont( new Font("SansSerif", Font.PLAIN, 20));
        jLabel1.add(jTextField4);

        jTextArea4.setText("phone:");
        jTextArea4.setFont(new Font("SansSerif", Font.LAYOUT_NO_START_CONTEXT, 20));
        jTextArea4.setBounds(20,321,100,30);
        jTextArea4.setBackground(Color.cyan);
        jTextArea4.setForeground(Color.BLACK);
        jLabel1.add(jTextArea4);
//===========================================================================================================
        jTextField5.setBounds(120,380,420,50);
        jTextField5.setFont( new Font("SansSerif", Font.PLAIN, 20));
        jLabel1.add(jTextField5);

        jTextArea5.setText("Email:");
        jTextArea5.setFont(new Font("SansSerif", Font.LAYOUT_NO_START_CONTEXT, 20));
        jTextArea5.setBounds(20,392,100,30);
        jTextArea5.setBackground(Color.cyan);
        jTextArea5.setForeground(Color.BLACK);
        jLabel1.add(jTextArea5);

        jButton.setBounds(300,500,100,30);
        jButton.setBackground(Color.cyan);
        jLabel1.add(jButton);
    }


   /* public static void main(String[] args) {
        AddMember addMember= new AddMember();

    }*/

}
