package com.company;
import com.mongodb.*;

import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoCollection;

import org.bson.Document;

import java.util.*;

import com.mongodb.client.MongoCursor;
import static com.mongodb.client.model.Filters.*;
import com.mongodb.client.result.DeleteResult;
import static com.mongodb.client.model.Updates.*;
import com.mongodb.client.result.UpdateResult;

public class Test {

    public static void main(String[] args) {
       Test test= new Test();
       test.test();
    }
    //================================================================ name of dbs and name of all collections

    public void test(){

        Mongo mongo = new Mongo("localhost", 27017);

        List<String> dbs = mongo.getDatabaseNames();
        for(String db : dbs){
            System.out.println("name of Data Base:"+db);
        }

        DB db = mongo.getDB("library");
        Set<String> tables = db.getCollectionNames();

        for(String coll : tables){
            System.out.println("name of collections:"+coll);
        }

        //=========================================================== insert documnet

       // Mongo mongo = new Mongo("localhost", 27017);
      //  DB db = mongo.getDB("library");
        DBCollection library = db.getCollection("books");

        BasicDBObject document = new BasicDBObject();
        document.put("_id", "3");
        document.put("name", "compiler");
        document.put("Date", "9 bahman 1368");
        document.put("writer", "Dr momtazi");
        document.put("place", 32);
        document.put("status", "borrowed");
        document.put("DateOfBorrow", new Date());
        document.put("user", "hamed");
        library.insert(document);


        //============================================================== update document



        BasicDBObject query = new BasicDBObject();
        query.put("name", "dataBase");

        BasicDBObject newDocument = new BasicDBObject();
        newDocument.put("status ", "bbbbbbb");
        newDocument.put("DateOfBorrow",new Date());

        BasicDBObject updateObj = new BasicDBObject();
        updateObj.put("$set", newDocument);

        library.update(query, updateObj);

        //===================================================================== find documnet with name= compiler

      /*  Mongo mongo = new Mongo("localhost", 27017);
        DB db = mongo.getDB("library");
        DBCollection library = db.getCollection("books");

        BasicDBObject searchQuery = new BasicDBObject();
        searchQuery.put("name", "compiler");

        DBCursor cursor = library.find(searchQuery);

        while (cursor.hasNext()) {
            System.out.println(cursor.next());
        }*/

        //========================================================================= delete documnet with name=...

      /*  BasicDBObject searchQuery = new BasicDBObject();  //bala ye bar goftim

        searchQuery.put("name", "compiler");

        library.remove(searchQuery);*/
       //==========================================================================



    }



}
