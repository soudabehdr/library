package com.company;

import javax.swing.*;
import java.awt.*;

public class UpdateBook extends JFrame {

    JLabel jLabel1 = new JLabel();

    JTextField jTextField1= new JTextField();
    JTextArea jTextArea1= new JTextArea();

    JTextField jTextField2= new JTextField();
    JTextArea jTextArea2= new JTextArea();

    JTextField jTextField3= new JTextField();
    JTextArea jTextArea3= new JTextArea();

    JTextField jTextField4= new JTextField();
    JTextArea jTextArea4= new JTextArea();

    JButton jButton= new JButton("ok");


    public UpdateBook(){

        JFrame jFrame= new JFrame();
        jFrame.setLayout(null);
        jFrame.setBounds(300,0,1200,800);
        jFrame.setLayout(new GridLayout(1,2));
        jFrame.setTitle("Library Management System");
        jFrame.setVisible(true);
        jFrame.add(jLabel1);



        jTextField1.setBounds(140,100,420,50);
        jTextField1.setFont( new Font("SansSerif", Font.PLAIN, 20));
        jLabel1.add(jTextField1);


        jTextArea1.setText("name:");
        jTextArea1.setFont(new Font("SansSerif", Font.LAYOUT_NO_START_CONTEXT, 20));
        jTextArea1.setBounds(20,112,100,30);
        jTextArea1.setBackground(Color.cyan);
        jTextArea1.setForeground(Color.BLACK);
        jLabel1.add(jTextArea1);
//===========================================================================================================
        jTextField2.setBounds(140,170,420,50);
        jTextField2.setFont( new Font("SansSerif", Font.PLAIN, 20));
        jLabel1.add(jTextField2);


        jTextArea2.setText("author:");
        jTextArea2.setFont(new Font("SansSerif", Font.LAYOUT_NO_START_CONTEXT, 20));
        jTextArea2.setBounds(20,182,100,30);
        jTextArea2.setBackground(Color.cyan);
        jTextArea2.setForeground(Color.BLACK);
        jLabel1.add(jTextArea2);
//===========================================================================================================

        jTextField3.setBounds(140,240,420,50);
        jTextField3.setFont( new Font("SansSerif", Font.PLAIN, 20));
        jLabel1.add(jTextField3);

        jTextArea3.setText("mUsername");
        jTextArea3.setFont(new Font("SansSerif", Font.LAYOUT_NO_START_CONTEXT, 20));
        jTextArea3.setBounds(20,252,150,30);
        jTextArea3.setBackground(Color.cyan);
        jTextArea3.setForeground(Color.BLACK);
        jLabel1.add(jTextArea3);
//============================================================================================================
        jTextField4.setBounds(140,310,420,50);
        jTextField4.setFont( new Font("SansSerif", Font.PLAIN, 20));
        jLabel1.add(jTextField4);

        jTextArea4.setText("status:");
        jTextArea4.setFont(new Font("SansSerif", Font.LAYOUT_NO_START_CONTEXT, 20));
        jTextArea4.setBounds(20,321,100,30);
        jTextArea4.setBackground(Color.cyan);
        jTextArea4.setForeground(Color.BLACK);
        jLabel1.add(jTextArea4);
//=============================================================================================================

        jButton.setBounds(300,500,100,30);
        jButton.setBackground(Color.cyan);
        jLabel1.add(jButton);

    }

    public static void main(String[] args) {
        UpdateBook updateBook= new UpdateBook();
    }
}
